# -*- coding: utf-8 -*-

from kivy.app import App
from kivy import Logger
from kivy.metrics import dp
from kivy.properties import ObjectProperty, DictProperty, StringProperty
from kivy.uix.widget import Widget
from kivymd.resources import get_color_value, get_text_color_value


default_theme = {
    # name of theme
    'name': 'default_theme',
    # colors (not hex)
    'colors': {
        'Primary': {
            'main': get_color_value('Blue', '500'),
            'light': get_color_value('Blue', '400'),
            'dark': get_color_value('Blue', '600'),
            'on': get_text_color_value('Blue', '500'),
            'disabled': (.2, .2, .8)
        },
        'Secondary': {
            'main': get_color_value('Orange', '500'),
            'light': get_color_value('Orange', '400'),
            'dark': get_color_value('Orange', '600'),
            'on': get_text_color_value('Orange', '500'),
            'disabled': (.8, .8, .2)
        },
        'Background': {
            'main': get_color_value('Gray', '500'),
            'light': get_color_value('Gray', '400'),
            'dark': get_color_value('Gray', '600'),
            'on': get_text_color_value('Gray', '500'),
            'disabled': (.2, .2, .2)
        },
        'Surface': {
            'main': get_color_value('Gray', '500'),
            'light': get_color_value('Gray', '400'),
            'dark': get_color_value('Gray', '600'),
            'on': get_text_color_value('Gray', '500'),
            'disabled': (.2, .2, .2)
        },
        'Error': {
            'main': get_color_value('Red', '500'),
            'light': get_color_value('Red', '400'),
            'dark': get_color_value('Red', '600'),
            'on': get_text_color_value('Red', '500'),
            'disabled': (.8, .2, .2)
        }
    },
    # font styles
    'font_styles': {
        # font, size, caps, spacing
        'H1': ['RobotoLight', 96, False, -1.5],
        'H2': ['RobotoLight', 60, False, -0.5],
        'H3': ['Roboto', 48, False, 0],
        'H4': ['Roboto', 34, False, 0.25],
        'H5': ['Roboto', 24, False, 0],
        'H6': ['RobotoMedium', 20, False, 0.15],
        'Subtitle1': ['Roboto', 16, False, 0.15],
        'Subtitle2': ['RobotoMedium', 14, False, 0.1],
        'Body1': ['Roboto', 16, False, 0.5],
        'Body2': ['Roboto', 14, False, 0.25],
        'Button': ['RobotoMedium', 14, True, 1.25],
        'Caption': ['Roboto', 12, False, 0.4],
        'Overline': ['Roboto', 10, True, 1.5],
        'Icon': ['Icons', 24, False, 0]
    },
    'Components': {
        'Button': {
            'height': dp(36),
            'min_width': dp(48),
            'ripple': True
        }
    },
    # ripple
    'ripple': {
        'color': get_color_value('Gray', '300')
    }
}


class ThemeManager(Widget):
    themes = DictProperty()
    main_theme = DictProperty()

    def __init__(self, theme=None, theme_name=None, **kwargs):
        self.themes['default_theme'] = default_theme
        if theme:
            self.add_theme(theme)
            self.main_theme = self.themes[theme['name']]
        elif theme_name:
            if theme_name in self.themes:
                self.main_theme = self.themes[theme_name]
            else:
                Logger.error('KivyMD: Theme "{}" not found'.format(theme_name))
        else:
            self.main_theme = self.themes['default_theme']
        super(ThemeManager, self).__init__(**kwargs)

    def add_theme(self, theme=None):
        if theme:
            self.themes[theme['name']] = theme

    def get_color(self, color='Primary', ctype='main', theme_name='main'):
        if theme_name == 'main':
            return self.main_theme['theme_colors'][color][ctype]
        else:
            return self.themes[theme_name]['theme_colors'][color][ctype]


class ThemeBehavior(Widget):
    tm = ObjectProperty(None)

    def __init__(self, **kwargs):
        if self.tm is None:
            if hasattr(App.get_running_app(), 'tm'):
                self.tm = App.get_running_app().tm
            else:
                self.tm = ThemeManager()
        super(ThemeBehavior, self).__init__(**kwargs)


class ComponentThemeBehavior(ThemeBehavior):
    theme_name = StringProperty('default_theme')
    theme = DictProperty()
    theme_component_name = StringProperty()
    theme_component = DictProperty()

    def __init__(self, **kwargs):
        super(ComponentThemeBehavior, self).__init__(**kwargs)
        self.tm.bind(themes=self.update_theme())
        self.bind(theme=self.update_theme_component())

    def update_theme(self):
        if self.theme_name in self.tm.themes.keys():
            self.theme = self.tm.themes[self.theme_name]

    def update_theme_component(self):
        if self.theme:
            if self.theme_component_name in self.theme['Components'].keys():
                self.theme_component =\
                    self.theme['Components'][self.theme_component_name]


class ScreenThemeBehavior(ThemeBehavior):
    theme_name = StringProperty('default_theme')
    theme = DictProperty()

    def __init__(self, **kwargs):
        super(ScreenThemeBehavior, self).__init__(**kwargs)

    def update_theme(self):
        if self.theme_name in self.tm.themes.keys():
            self.theme = self.tm.themes[self.theme_name]
