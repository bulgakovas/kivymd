# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.metrics import sp
from kivy.properties import OptionProperty, ListProperty

from kivy.uix.label import Label
from kivymd.theme import ThemeBehavior
from kivymd.resources import theme_font_styles, theme_colors


Builder.load_string('''
<MDLabel>:
    text_size: (self.width, None)
''')


class MDLabel(ThemeBehavior, Label):
    font_style = OptionProperty('Body1', options=theme_font_styles)
    theme_text_color = OptionProperty('On_Primary', options=theme_colors)
    text_color = ListProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super(MDLabel, self).__init__(**kwargs)
        self.on_font_style(None, self.font_style)

    def on_font_style(self, instance, value):
        if not self.font_style == 'Custom':
            font_info = self.tm.main_theme\
                ['font_styles'][self.font_style]
            self.font_name = font_info[0]
            self.font_size = sp(font_info[1])
            # TODO: Add letter spacing change
            self.on_text(None, self.text)
        else:
            self.on_text_color(None, self.text_color)

    def on_text(self, instance, value):
        if self.tm.main_theme['font_styles'][self.font_style][3]\
                and not self.text == self.text.upper():
            self.text = self.text.upper()

    def on_theme_text_color(self, instance, value):
        if not self.theme_text_color == 'Custom':
            self.color = self.tm.main_theme\
                ['colors'][self.theme_text_color]

    def on_text_color(self, instance, value):
        if self.theme_text_color == 'Custom':
            self.color = self.text_color
