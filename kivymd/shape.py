# -*- coding: utf-8 -*-

import re

from kivy.logger import Logger
from kivy.uix.widget import Widget
from kivy.properties import ListProperty, AliasProperty, OptionProperty, \
    ObjectProperty, NumericProperty, BooleanProperty, StringProperty
from kivy.metrics import pt, inch, cm, mm, dp, sp
from kivy.animation import Animation
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Line, RoundedRectangle, Ellipse
from kivy.graphics.stencil_instructions import StencilPush, StencilUse, \
    StencilUnUse, StencilPop


class RippleBehavior(Widget):
    # TODO: Calculating speed by widget size
    ripple_rad_default = NumericProperty(1)
    ripple_duration_start_in = NumericProperty(2)
    ripple_duration_finish_in = NumericProperty(.3)
    ripple_duration_out = NumericProperty(.3)
    ripple_fade_from_alpha = NumericProperty(.5)
    ripple_fade_to_alpha = NumericProperty(.8)
    ripple_scale = NumericProperty(2)
    ripple_func_in = StringProperty('out_quad')
    ripple_func_out = StringProperty('out_quad')
    ripple_rad = NumericProperty(1)
    ripple_rad_finish = NumericProperty(1)
    ripple_pos = ListProperty([0, 0])
    ripple_color = ListProperty([.8, .8, .8, .5])

    ripple_doing = BooleanProperty(False)
    ripple_finishing = BooleanProperty(False)
    ripple_fading_out = BooleanProperty(False)

    ripple_ellipse = ObjectProperty()
    ripple_ellipse_color = None

    ripple = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(RippleBehavior, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        if not self.ripple or not self.collide_point(touch.x, touch.y) or\
                touch.is_mouse_scrolling:
            return False

        if not self.disabled:
            self.ripple_rad = self.ripple_rad_default
            self.ripple_pos = (touch.x, touch.y)
            if not self.ripple_color == []:
                pass
            elif hasattr(self, 'tm'):
                self.ripple_color = self.tm.main_theme\
                    ['ripple']['color']
            self.ripple_color[3] = self.ripple_fade_from_alpha

            self.lay_canvas_instructions()
            self.ripple_rad_finish = max(self.width, self.height) *\
                self.ripple_scale
            self.start_ripple()
        return super(RippleBehavior, self).on_touch_down(touch)

    def on_touch_move(self, touch, *args):
        if not self.collide_point(touch.x, touch.y)\
           and self.ripple_doing and not self.ripple_finishing:
                self.finish_ripple()
        return super(RippleBehavior, self).on_touch_move(touch, *args)

    def on_touch_up(self, touch):
        if self.collide_point(touch.x, touch.y) and self.ripple_doing:
            self.finish_ripple()
        return super(RippleBehavior, self).on_touch_move(touch)

    def lay_canvas_instructions(self):
        raise NotImplementedError

    def start_ripple(self):
        if not self.ripple_doing:
            anim = Animation(
                ripple_rad=self.ripple_rad_finish,
                t='linear',
                duration=self.ripple_duration_start_in
            )
            anim.bind(on_complete=self.fade_out)
            self.ripple_doing = True
            anim.start(self)

    def finish_ripple(self):
        if self.ripple_doing and not self.ripple_finishing:
            Animation.cancel_all(self, 'ripple_rad')
            anim = Animation(
                ripple_rad=self.ripple_rad_finish,
                t=self.ripple_func_in,
                duration=self.ripple_duration_finish_in
            )
            anim.bind(on_complete=self.fade_out)
            self.ripple_finishing = True
            anim.start(self)

    def fade_out(self, *args):
        if not self.ripple_fading_out:
            rc = self.ripple_color
            Animation.cancel_all(self, 'ripple_color')
            anim = Animation(
                ripple_color=[rc[0], rc[1], rc[2], 0],
                t=self.ripple_func_out,
                duration=self.ripple_duration_out
            )
            anim.bind(on_complete=self.anim_complete)
            self.ripple_fading_out = True
            anim.start(self)

    def anim_complete(self, *args):
        self.ripple_doing = False
        self.ripple_finishing = False
        self.ripple_fading_out = False
        self.canvas.after.clear()

    def _set_ellipse(self, instance, value):
        self.ripple_ellipse.size = (self.ripple_rad, self.ripple_rad)

    def _set_color(self, instance, value):
        self.ripple_ellipse_color.a = value[3]


class Shape(RippleBehavior):

    def _get_corners(self):
        return self._corners

    def _set_corners(self, value):
        if isinstance(value, str):
            value = [value]
        vallen = len(value)
        if vallen == 1:
            self._corners = [value[0], value[0], value[0], value[0]]
        elif vallen == 2:
            self._corners = [value[0], value[1], value[1], value[0]]
        elif vallen == 3:
            self._corners = [value[0], value[1], value[2], value[0]]
        elif vallen == 4:
            self._corners = [value[0], value[1], value[2], value[3]]
        self.corners_update(None, self._corners)

    corners = AliasProperty(_get_corners, _set_corners)
    _corners = ListProperty(['Rounded 6dp', 'Rounded 6dp',
                             'Rounded 6dp', 'Rounded 6dp'])
    shape = ListProperty([2, 6, 2, 6, 2, 6, 2, 6])
    _shape = ObjectProperty()
    _mask = ObjectProperty()

    corners_types = ListProperty(['Sharp', 'Cut', 'Rounded'])
    style = OptionProperty('Transparent', options=['Contained', 'Outlined',
                                                   'Transparent'])
    shape_color = ListProperty([0, 1, 0, 1])

    def __init__(self, **kwargs):
        super(Shape, self).__init__(**kwargs)
        self.bind(style=self.create_shape, pos=self.create_shape,
                  size=self.create_shape)
        self.corners_update(None, self._corners)

    def parse_corner(self, elem):
        stype, ssize = elem.split(' ', 2)
        ctype = 0
        if stype in self.corners_types:
            ctype = self.corners_types.index(stype)
        else:
            Logger.error('KivyMD: Corner type "{}" not found. Using'
                         '"Rounded"'.format(stype))
            ctype = 2
        csizegiven = int(re.findall(r'^\d+', ssize)[0])
        csizemetrics = re.findall(r'[^ 0-9]+$', ssize)[0]
        csize = 0
        if csizemetrics == 'pt':
            csize = pt(csizegiven)
        elif csizemetrics == 'inch':
            csize = inch(csizegiven)
        elif csizemetrics == 'cm':
            csize = cm(csizegiven)
        elif csizemetrics == 'mm':
            csize = mm(csizegiven)
        elif csizemetrics == 'dp':
            csize = dp(csizegiven)
        elif csizemetrics == 'sp':
            csize = sp(csizegiven)
        else:
            csize = csizegiven
        csize = int(csize)
        return ctype, csize

    def corners_update(self, instance, value):
        # top-left
        tlt, tls = self.parse_corner(self._corners[0])
        # top-right
        trt, trs = self.parse_corner(self._corners[1])
        # bottom-right
        brt, brs = self.parse_corner(self._corners[2])
        # bottom-left
        blt, bls = self.parse_corner(self._corners[3])
        self.shape = [tlt, tls, trt, trs, brt, brs, blt, bls]
        self.create_shape()

    def create_shape(self, *args):
        shape = self.shape
        style = self.style
        # TODO: Create different styles of corners
        if shape[0] == 0:  # Sharp
            # TODO: Create sharp corner
            pass
        elif shape[0] == 1:  # Cut
            # TODO: Create cut corner
            pass
        elif shape[0] == 2:  # Rounded
            self._mask = RoundedRectangle(pos=self.pos, size=self.size,
                                          radius=[shape[1],  shape[3],
                                                  shape[5], shape[7]])
            if style == 'Contained':
                self._shape = self._mask
            elif style == 'Outlined':
                self._shape = Line(rounded_rectangle=[self.x, self.y,
                                   self.width, self.height, shape[1], shape[3],
                                   shape[5], shape[7]])
        self.draw_shape()

    def draw_shape(self):
        self.canvas.before.clear()
        if not self.style == 'Transparent':
            self.canvas.before.add(Color(rgba=self.shape_color))
            self.canvas.before.add(self._shape)

    def lay_canvas_instructions(self):
        with self.canvas.after:
            StencilPush()
            self.canvas.after.add(self._mask)
            StencilUse()
            self.ripple_ellipse_color = Color(rgba=self.ripple_color)
            self.ripple_ellipse = \
                Ellipse(size=(self.ripple_rad, self.ripple_rad),
                        pos=(self.ripple_pos[0] - self.ripple_rad / 2.,
                             self.ripple_pos[1] - self.ripple_rad / 2.))
            StencilUnUse()
            self.canvas.after.add(self._mask)
            StencilPop()
        self.bind(ripple_color=self._set_color,
                  ripple_rad=self._set_ellipse)

    def _set_ellipse(self, instance, value):
        super(Shape, self)._set_ellipse(instance, value)
        self.ripple_ellipse.pos = (self.ripple_pos[0] - self.ripple_rad / 2.,
                                   self.ripple_pos[1] - self.ripple_rad / 2.)
