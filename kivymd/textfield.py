# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import StringProperty, BooleanProperty, ListProperty,\
    NumericProperty, OptionProperty
from kivy.animation import Animation

from kivy.uix.textinput import TextInput

from kivymd.theme import ThemeBehavior
from kivymd.label import MDLabel


Builder.load_string('''
<MDTextField>:
    canvas.before:
        Clear
        # Line
        Color:
            rgba: self.inactive_color
        Line:
            points:
                self.x, self.y + dp(16), self.x + self.width, self.y + dp(16)
            width: 1
            dash_length: dp(3)
            dash_offset: 2 if self.disabled else 0
        # Line
        Color:
            rgba: self._current_line_color
        Rectangle:
            size: self._line_width, dp(2)
            pos: self.center_x - (self._line_width / 2), self.y + dp(16)
        # Length
        Color:
            rgba: self._current_length_lbl_color
        Rectangle:
            texture: self._length_lbl.texture
            size: self._length_lbl.texture_size
            pos: self.width - self._length_lbl.texture_size[0], self.y
        # Helper
        Color:
            rgba: self._current_helper_lbl_color
        Rectangle:
            texture: self._helper_lbl.texture
            size: self._helper_lbl.texture_size
            pos: self.x, self.y
        # Hint text
        Color:
            rgba: self._current_hint_lbl_color
        Rectangle:
            texture: self._hint_lbl.texture
            size: self._hint_lbl.texture_size
            pos: self.x, self.y + self.height - self._hint_y
        # Cursor blink
        Color:
            rgba:
                (self._current_line_color if self.focus and not \
                self._cursor_blink else (0, 0, 0, 0))
        Rectangle:
            pos: [int(x) for x in self.cursor_pos]
            size: 1, -self.line_height
        # Text color
        Color:
            rgba:
                self.foreground_color

    font_name: 'Roboto'
    foreground_color: app.tm.main_theme['colors']['Background']['on']
    font_size: sp(16)
    bold: False
    padding: 0, dp(16), 0, dp(10)
    multiline: False
    size_hint_y: None
    height: self.minimum_height + dp(8)
''')


class MDTextField(ThemeBehavior, TextInput):
    hint_text = StringProperty('')
    helper_text = StringProperty('')
    helper_text_mode = OptionProperty('none', options=['none', 'persistent',
                                                       'on_error', 'on_focus'])
    max_text_length = NumericProperty(0)
    length_text_mode = OptionProperty('on_focus', options=['none', 'persistent',
                                                           'on_error',
                                                           'on_focus'])

    main_color = ListProperty()
    inactive_color = ListProperty()
    activated_color = ListProperty()
    focused_color = ListProperty()
    hover_color = ListProperty()
    error_color = ListProperty()
    disabled_color = ListProperty()
    state = OptionProperty('Inactive', options=['Inactive', 'Activated',
                                                'Focused', 'Hover', 'Error',
                                                'Disabled'])

    required = BooleanProperty(False)
    error = BooleanProperty(False)

    _error_len = BooleanProperty(False)
    _error_required = BooleanProperty(False)
    _error = BooleanProperty(False)

    _hint_lbl_font_size = NumericProperty('16dp')
    _hint_y = NumericProperty('38dp')
    _line_width = NumericProperty(0)
    _current_line_color = ListProperty([0.0, 0.0, 0.0, 0.0])
    _current_helper_lbl_color = ListProperty([0.0, 0.0, 0.0, 0.0])
    _current_hint_lbl_color = ListProperty([0.0, 0.0, 0.0, 0.0])
    _current_length_lbl_color = ListProperty([0.0, 0.0, 0.0, 0.0])

    def __init__(self, **kwargs):
        super(MDTextField, self).__init__(**kwargs)
        # Labels
        self._hint_lbl = MDLabel(font_style='Subtitle1',
                                 halign='left',
                                 valign='middle',
                                 text=self.hint_text)

        self._helper_lbl = MDLabel(font_style='Caption',
                                   halign='left',
                                   valign='middle',
                                   text=self.helper_text)

        self._length_lbl = MDLabel(font_style='Caption',
                                   halign='right',
                                   valign='middle',
                                   text="")

        # Colors
        self.inactive_color = [0, 0, 0, .6]
        self.activated_color = self.tm.main_theme['colors']['Primary']['main']
        self.focused_color = [0, 0, 0, 1]
        self.hover_color = [0, 0, 0, 1]
        self.error_color = [.6875, 0, .1254, 1]
        self.disabled_color = [0, 0, 0, .12]
        self.main_color = self.inactive_color
        self._current_line_color = self.inactive_color
        self.selection_color = [self.activated_color[0],
                                self.activated_color[1],
                                self.activated_color[2],
                                self.activated_color[3] * .5]

        # Bind
        self.bind(text=self.on_text,
                  max_text_length=self._set_max_text_length,
                  hint_text=self._set_hint,
                  helper_text=self._set_helper,
                  helper_text_mode=self._set_helper_mode)

        self.set_state()

    def on__hint_text(self, instance, value):
        pass

    def _refresh_hint_text(self):
        pass

    def _check_max_text_length(self):
        if 0 < self.max_text_length < len(self.text):
            self._error_len = True
        else:
            self._error_len = False
        return self._error_len

    def _check_required_text(self):
        if self.required and len(self.text) == 0:
            self._error_required = True
        else:
            self._error_required = False
        return self._error_required

    def _check_any_error(self):
        error_user = self.error
        error_length = self._check_max_text_length()
        error_required = self._check_required_text()
        self._error = error_user or error_length or error_required
        return self._error

    def set_state(self):
        # states
        self._check_any_error()
        if self.disabled:
            self.state = 'Disabled'
        elif self._error:
            self.state = 'Error'
        elif self.focus:
            if len(self.text) > 0:
                self.state = 'Activated'
            else:
                self.state = 'Activated'  # 'Focused'
        # elif self.hover:
        #    self.state = 'Hover'
        else:
            self.state = 'Inactive'

        # line, hint color
        if self.state == 'Disabled':
            line_color = self.disabled_color
            hint_color = self.disabled_color
        elif self.state == 'Error':
            line_color = self.error_color
            hint_color = self.error_color
        elif self.state == 'Focused':
            line_color = self.focused_color
            hint_color = self.inactive_color
        elif self.state == 'Activated':
            line_color = self.activated_color
            hint_color = self.activated_color
        else:  # 'Inactive'
            line_color = self.inactive_color
            hint_color = self.inactive_color

        # helper color
        if self.helper_text_mode == 'persistent':
            helper_color = hint_color
        elif self.helper_text_mode == 'on_error' and self._error:
            helper_color = self.error_color
        elif self.helper_text_mode == 'on_error' and not self._error:
            helper_color = [0, 0, 0, 0]
        elif self.helper_text_mode == 'on_focus' and self.focus:
            helper_color = hint_color
        elif self.helper_text_mode == 'on_focus' and not self.focus:
            helper_color = [0, 0, 0, 0]
        else:
            helper_color = [0, 0, 0, 0]

        # length color
        if self.length_text_mode == 'persistent':
            length_color = hint_color
        elif self.length_text_mode == 'on_error' and self._error_len:
            length_color = self.error_color
        elif self.length_text_mode == 'on_error' and not self._error_len:
            length_color = [0, 0, 0, 0]
        elif self.length_text_mode == 'on_focus' and self.focus:
            length_color = hint_color
        elif self.length_text_mode == 'on_focus' and not self.focus:
            length_color = [0, 0, 0, 0]
        else:
            length_color = [0, 0, 0, 0]

        # hint y, font_size
        if len(self.text) == 0 and not self.focus:
            hint_y = dp(38)
            hint_font_size = dp(16)
        else:
            if not self.focus:
                hint_y = None
                hint_font_size = None
                self._hint_y = dp(14)
                self._hint_lbl_font_size = dp(12)
            else:
                hint_y = dp(14)
                hint_font_size = dp(12)

        # line width
        if self.focus:
            line_width = self.width
        else:
            line_width = 0

        # animations
        if line_width is not None and\
                not line_width == self._line_width:
            Animation.cancel_all(self, '_line_width')
            Animation(duration=.2,
                      t='out_quad',
                      _line_width=line_width).start(self)
        if line_color is not None and\
                not line_color == self._current_line_color:
            Animation.cancel_all(self, '_current_line_color')
            Animation(duration=.2,
                      _current_line_color=line_color).start(self)
        if hint_color is not None and\
                not hint_color == self._current_hint_lbl_color:
            Animation.cancel_all(self, '_current_hint_lbl_color')
            Animation(duration=.2,
                      _current_hint_lbl_color=hint_color).start(self)
        if helper_color is not None and\
                not helper_color == self._current_helper_lbl_color:
            Animation.cancel_all(self, '_current_helper_lbl_color')
            Animation(duration=.2,
                      _current_helper_lbl_color=helper_color).start(self)
        if length_color is not None and\
                not length_color == self._current_length_lbl_color:
            Animation.cancel_all(self, '_current_length_lbl_color')
            Animation(duration=.2,
                      _current_length_lbl_color=length_color).start(self)
        if hint_y is not None and\
                not hint_y == self._hint_y:
            Animation.cancel_all(self, '_hint_y')
            Animation(duration=.2,
                      t='out_quad',
                      _hint_y=hint_y).start(self)
        if hint_font_size is not None and\
                not hint_font_size == self._hint_lbl_font_size:
            Animation.cancel_all(self, '_hint_lbl_font_size')
            Animation(duration=.2,
                      t='out_quad',
                      _hint_lbl_font_size=hint_font_size).start(self)

    def on_focus(self, instance, value):
        self.set_state()

    def on_text(self, instance, value):
        self._set_length_lbl_text()
        self.set_state()

    def on_disabled(self, instance, value):
        self.set_state()

    def on_width(self, instance, value):
        if self._line_width > 0:
            self._line_width = self.width
        self._length_lbl.width = self.width
        self._hint_lbl.width = self.width
        self._helper_lbl.width = self.width

    def _set_length_lbl_text(self):
        if self.max_text_length > 0:
            self._length_lbl.text = '{}/{}'.format(len(self.text),
                                                   self.max_text_length)
        else:
            self._length_lbl.text = ''

    def _set_max_text_length(self, instance, value):
        self.max_text_length = value
        self._set_length_lbl_text()

    def _set_hint(self, instance, value):
        self._hint_lbl.text = value

    def on__hint_lbl_font_size(self, instance, value):
        self._hint_lbl.font_size = value

    def _set_helper(self, instance, value):
        self._helper_lbl.text = value
        self.helper_text = value

    def _set_helper_mode(self, instance, value):
        self.helper_text_mode = value
