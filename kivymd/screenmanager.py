# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen

from kivymd.theme import ThemeBehavior, ScreenThemeBehavior


class MDScreenManager(ScreenManager, ThemeBehavior):

    def __init__(self, **kwargs):
        super(MDScreenManager, self).__init__(**kwargs)
        self.app = App.get_running_app()


class MDScreen(Screen, ScreenThemeBehavior):

    def __init__(self, **kwargs):
        super(MDScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()
