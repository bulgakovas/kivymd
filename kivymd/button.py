# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty

from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.behaviors import ButtonBehavior

from kivymd.shape import Shape
from kivymd.theme import ThemeBehavior


Builder.load_string('''
#:import MDLabel kivymd.label.MDLabel

<BaseButton>:
    size_hint: (None, None)
    anchor_x: 'center'
    anchor_y: 'center'

<MDButton>:
    height: self.md_height
    width: self.md_width
    content: content
    MDLabel:
        id: content
        text: root.text
        size_hint_x: None
        text_size: (None, root.height)
        height: self.texture_size[1]
        font_style: 'Button'
        valign: 'middle'
        halign: 'center'
        disabled: root.disabled
''')


class BaseButton(ThemeBehavior, ButtonBehavior, AnchorLayout):

    def __init__(self, **kwargs):
        super(BaseButton, self).__init__(**kwargs)


class MDButton(BaseButton, Shape):
    text = StringProperty('')
    md_height = NumericProperty('36dp')
    md_width = NumericProperty('64dp')

    def __init__(self, **kwargs):
        super(MDButton, self).__init__(**kwargs)
