# -*- coding: utf-8 -*-

__version_info__ = (0, 1, 1)
__version__ = '0.1.1'

from kivy import Logger
Logger.info('KivyMD: Version: {}'.format(__version__))
