# -*- coding: utf-8 -*-

from distutils.core import setup
from kivymd.__init__ import __version__

setup(
    name='kivymd',
    version=__version__,
    description='KivyMD is a set of widgets for Kivy '
                'corresponding to Google\'s Material Design',
    long_description=open('README.md').read(),
    license=open('LICENSE.md').read(),
    author='Artem Bulgakov',
    author_email='bulgakov-a-s@yandex.ru',
    url='https://bitbucket.org/bulgakovas/kivymd',
    packages=['kivymd'],
    requires=['kivy']
)
