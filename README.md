# KivyMD

KivyMD is a set of widgets for [Kivy](https://kivy.org/) corresponding to Google's [Material Design](https://material.io/design/introduction/)

## Installation

#### You need
* Python ([Download](https://www.python.org/downloads/))
* Kivy ([Installation](https://kivy.org/doc/stable/installation/installation.html))

KivyMD is being developed on the newest versions.

#### Install

Clone the repository and run setup.py script:

    python ./setup.py install


## Author

Artem Bulgakov


## License

MIT [License](LICENSE.md), same as Kivy.

[Material Design Iconic Font](https://zavoloklom.github.io/material-design-iconic-font/index.html) covered by [this licenses](https://zavoloklom.github.io/material-design-iconic-font/license.html).